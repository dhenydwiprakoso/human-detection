import pandas as pd

data = pd.read_csv("output.csv")
waktu_baru = []

for time in data['Waktu']:
    waktu_baru.append(time[:10])
data['Day'] = waktu_baru
data_new = data.drop('Waktu', axis=1)

data_w_count = data_new.groupby(['x','y','Day']).size().reset_index(name='Count')
data_w_count.to_csv("output-with-value.csv", index=False)