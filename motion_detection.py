from picamera.array import PiRGBArray
from picamera import PiCamera
import datetime
import imutils
import time
import cv2
import warnings
import pandas as pd
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np

#class tracking menggunakan centroid
class CentroidTracker():
    def __init__(self, maxDisappeared=0):
        #inisiasi object id
        self.nextObjectID=0
        self.objects = OrderedDict() #untuk simpan id dan centroid
        self.disappeared = OrderedDict() #untuk tau berapa banyak frame obj hilang
        
        #max object ilang dari layar agar id di-deregister
        self.maxDisappeared = maxDisappeared
        
    #register yang sebelumnya belum terdeteksi
    def register(self, centroid):
        self.objects[self.nextObjectID] = centroid
        self.disappeared[self.nextObjectID] = 0
        self.nextObjectID +=1
    #deregister object yang sudah beberapa frame tidak terdeteksi
    def deregister(self, objectID):
        del self.objects[objectID]
        del self.disappeared[objectID]
    
    def update(self, rects):
        #cek untuk lihat ada centroid atau tidak
        if len(rects) == 0:
            #loop object yang sudah teredektsi sebelumnya
            #lalu tanda disappear bertambah 1
            for objectID in list(self.disappeared.keys()):
                self.disappeared[objectID] +=1
                
                #kalau banyak object disappear lebih dari max, maka deregister
                if self.disappeared[objectID] > self.maxDisappeared:
                    self.deregister(objectID)
                
            return self.objects
        
        #inisiasi array input centroid
        inputCentroids = np.zeros((len(rects), 2), dtype="int")
        
    #loop bounding box rectangle
        for (i, (startX, startY, endX, endY)) in enumerate(rects):
            #pakai bounding box untuk mendapat centroid
            cX = int((startX + endX) / 2.0)
            cY = int((startY + endY) / 2.0)
            inputCentroids[i] = (cX, cY)

        #register semua objek kalau sebelumya belum pernah tracking apa-apa
        if len(self.objects) == 0:
            for i in range(0, len(inputCentroids)):
                self.register(inputCentroids[i])
        
        else:
            
            objectIDs = list(self.objects.keys())
            objectCentroids = list(self.objects.values())
            
            #hitung distance centroid sekarang dengan yang tersimpan/sudah ada id nya
            D = dist.cdist(np.array(objectCentroids), inputCentroids)
            
            #menemukan nilai terkecil pada setiap row lalu sort row index berdasarkan
            #minimum value
            rows = D.min(axis=1).argsort()
            #temukan nilai terkecil dari setiap kolom lalu sorting paai ordered rows
            cols = D.argmin(axis=1)[rows]
            
            usedRows = set()
            usedCols = set()
            
            for (row, col) in zip(rows, cols):
                
                #abaikan kalau row atau kolom udah diperiksa
                if row in usedRows or col in usedCols:
                    continue
                
                #ambil object id dari row sekarang, set centroid baru
                #reset disappeared counter
                objectID = objectIDs[row]
                self.objects[objectID]= inputCentroids[col]
                self.disappeared[objectID] = 0
                
                #masukin row dan col ke list yang sudah diperiksa
                usedRows.add(row)
                usedCols.add(col)
                
            #hitung row dan column yang belum digunakan/diperiksa
            unusedRows = set(range(0, D.shape[0])).difference(usedRows)
            unusedCols = set(range(0, D.shape[1])).difference(usedCols)
            
            #kalau jumlah dari object cetroid lebih dari sama dengan input centroid
            #perlu cek jika beberapa dari object ini punya kemungkinan disappeared
            if D.shape[0] >= D.shape[1]:
                #loop row index yang belum digunakan
                for row in unusedRows:
                    #ambil object id dan increment counter disappeared
                    objectID = objectIDs[row]
                    self.disappeared[objectID] +=1
                    
                    #kalau disappeared lebih dari udah lebih dari max, maka di-deregistrasi
                    if self.disappeared[objectID] > self.maxDisappeared:
                        self.deregister(objectID)
                #kalau number of input centroid lebih besar dari existing object centroid, maka register-kan jadi centroid baru    
            else:
                for col in unusedCols:
                    self.register(inputCentroids[col])
                
        return self.objects

#abaikan warning
warnings.filterwarnings("ignore")

#inisiasi centroid tracker
ct = CentroidTracker()

#minimum area yang bisa dideteksi menggunakan motion detection
min_area = 2000

#inisiasi kamera
camera = PiCamera()
camera.resolution = (640, 400)
camera.framerate=16
rawCapture = PiRGBArray(camera, size=(640,400))

#waktu untuk kamera pemanasan
time.sleep(1)

#inisiasi average background frame, frame motion counter
avg = None
lastUploaded = datetime.datetime.now()


#dataframe kosong untuk nantinya disimpan dalam file
data_csv = pd.DataFrame(columns=['Waktu','ID','x','y'])


#tangkap frame dari kamera
for f in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    #ambil raw numpy array yang mewakili gambar
    frame = f.array
    #ambil waktu sekarang
    timestamp = datetime.datetime.now()
    
    
    #resize frame, ubah ke grayscale lalu apply gaussian blur unruk mengurangi noise
    frame = imutils.resize(frame, width=500)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (21,21), 0)
    #simpan gambar awal ke file
    #inisiasi average frame saat masih None (berarti saat awal kamera menyala)
    if avg is None:
        cv2.imwrite("initial-image.jpg", frame)
        avg = gray.copy().astype("float")
        rawCapture.truncate(0)
        continue
    #akulumasi weighted average antara frame sekarang dengan frame sebelumnya
    #lalu hitung perbandingannya
    cv2.accumulateWeighted(gray, avg, 0.7)
    frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))
    
    #threshold delta image, dilate threshold image, temukan contour pada threshold image
    thresh = cv2.threshold(frameDelta, 3, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh,None,iterations= 3)
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    
    #list untuk menampung koordinat orang yang akan dideteksi
    x_list = []
    y_list = []
    rects = []
    list_id = []
    #loop contours
    for c in cnts:
        #abaikan contour kalau terlalu kecil
        if cv2.contourArea(c) < min_area:
            continue
        
        #hitung bounding box untuk contour lalu gambar di frame
        (x, y, w, h) = cv2.boundingRect(c)
        rects.append([x, y, x+w, y+h])
        cv2.rectangle(frame, (x,y), (x+w, y+h), (0,255,0), 2)
            
    #update cetroid tracker pakai bounding box
    objects = ct.update(rects)

    #loop tracked objects
    for (objectID, centroid) in objects.items():
        #gambar id dan centroid ke frame
        text = "ID {}".format(objectID)
        
        cv2.putText(frame, text, (centroid[0] - 10, centroid[1] - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.circle(frame, (centroid[0], centroid[1]), 4,(0, 255, 0), -1)
        
        #simpan list dan waktu ke dataframe
        x_list.append(centroid[0])
        y_list.append(centroid[1])
        list_id.append(objectID)
        ts = timestamp.strftime('%Y-%m-%d %H:%M:%S')
        dict_data = {'Waktu':ts, 'ID': list_id, 'x':x_list, 'y':y_list}
        data = pd.DataFrame(dict_data, columns=['Waktu','ID','x', 'y'])
        data_csv = data_csv.append(data, ignore_index=True)
    
    #tulis tabel padas ke file setiap 3 detik
    if (timestamp - lastUploaded).seconds >= 3:
        data_csv.to_csv("output.csv", index=False)
        lasUploaded = timestamp
        
    #show frame
    cv2.imshow("Kamera Raspberry pi", frame)
    key = cv2.waitKey(1) & 0xFF
    
    #clear stream untuk frame selanjutnya
    rawCapture.truncate(0)
    
    #break dari loop saat tombol "enter" ditekan
    if key== ord("\r"):
        break    

#tutup kamera raspi
camera.close()
#close window kamera
cv2.destroyAllWindows()