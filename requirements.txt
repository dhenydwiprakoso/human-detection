numpy==1.16.2
opencv-contrib-python==4.1.0.25
picamera==1.13
imutils==0.5.3
pandas==1.0.5
scipy==1.5.1
ipywidgets==7.5.1